# README #

Sample Rails application for show use RabbitMQ for Pub/Sub. (Build on article: http://www.codetunes.com/2014/event-sourcing-on-rails-with-rabbitmq/)

RabbitMQ bindings configuration:
```bash
cd blog
rake rabbitmq:setup
```